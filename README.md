# Hexagon heat map
## What is this project?
Web application using d3 and react to render U.S. state map. Utilize d3 transition to add animation.

## Tech Stack
* [React](https://facebook.github.io/react/)
* [Redux](http://redux.js.org/)
* [Babel](https://babeljs.io/)
* [Webpack](https://webpack.github.io/)
* [d3](https://d3js.org/)

## Implementation
Some of the code pattern is hacky in order to generate the d3 map. Data is randomly generated and store in the redux store. Then used to map to a d3 color scale.
The hexagon map is generated from a pre-render static map. The function reads the different color scale on the static map and draw hexagon based on the pixel values.

### Installation

To install dependencies you can use either `npm` or `yarn`.

```
npm install npm@3 -g
npm install
```
or
```
npm install yarn -g
yarn install
```

### Usage

```
# Start the app locally
NODE_ENV=development
npm start

# Start the app in production
NODE_ENV=production
npm run build
npm start

# Linting
npm run lint
```
