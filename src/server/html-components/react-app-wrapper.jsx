import React, { Component } from 'react';

export default class ReactAppWrapper extends Component {

    render() {
        return (
            <html lang="en-US">
                <head>
                    <title>Hexagon Heat Map</title>
                    <link rel="stylesheet" href="/public/style.css" />
                </head>
                <body>
                    <div id="app-container" />
                    <script type="text/javascript" src="/public/libs.js" />
                    <script type="text/javascript" src="/public/hexgon-heat-map.js" />
                </body>
            </html>
        );
    }
}
