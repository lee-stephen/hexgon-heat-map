// External libraries
import React from 'react';
import webpack from 'webpack';
import ReactDOMServer from 'react-dom/server';
import express from 'express';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import compression from 'compression';
import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';

import webpackConfig from '../../webpack.config';

// HTML component wrapper
import ReactAppWrapper from './html-components/react-app-wrapper';

const ENV = process.env.NODE_ENV || 'development';
const PORT = process.env.PORT || 8888; // For heroku only
const app = express();

app.use(compression());
app.use(bodyParser.json());
app.use(cookieParser());

if (ENV === 'development') {
    // local dev webpack building/serving
    const compiler = webpack(webpackConfig);
    app.use(webpackDevMiddleware(compiler, {
        hot: true,
        noInfo: true,
        publicPath: webpackConfig.output.publicPath,
        stats: {
            colors: true
        }
    }));
    app.use(webpackHotMiddleware(compiler, {
        log: console.log,
        path: '/__webpack_hmr',
        heartbeat: 10 * 1000,
    }));
    app.use('/public/images', express.static(`${__dirname}/../public/images`));
    console.log('webpack building...');
} else {
    app.use('/public', express.static(`${__dirname}/../../dist/public`));
}

// use express router
app.get('/', (req, res) => {
    const markup = ReactDOMServer.renderToStaticMarkup(
        <ReactAppWrapper />
    );

    res.status(200).send(markup);
});

console.log(`Listening on port ${PORT} [${ENV}]`);

app.listen(PORT);

module.exports = app;
