import { compose } from 'recompose';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import provideStore from '../hocs/provide-store';

// Action
import {
    generateMapData,
    toggleTransition,
    toggleMap
} from '../redux/actions/map-data';

// Store
import hexgonHeadMapAppStore from '../redux/store/hexgon-map-app';

const mapStateToProps = state => state.mapData;

const mapDispatchToProps = dispatch => ({
    generateNewData: bindActionCreators(generateMapData, dispatch),
    toggleTransition: bindActionCreators(toggleTransition, dispatch),
    toggleMap: bindActionCreators(toggleMap, dispatch)
});

export default compose(
    provideStore(hexgonHeadMapAppStore),
    connect(mapStateToProps, mapDispatchToProps)
);
