import { get } from 'lodash';

import us from '../data/us.json';

export const randomHelper = (max, min, digit = 0) => {
    const factor = digit === 0 ? 1 : 10 * digit;
    return Math.round(((Math.random() * (max - min)) + min) * factor) / factor;
};

export const generateRandomIdValue = (max = 10, min = 5) => {
    // save 1 digit on random number
    const getRandomNumber = () => randomHelper(max, min, 1);

    const counties = get(us.objects, 'counties.geometries');

    const value = {};
    counties.forEach(county => (
        value[county.id] = getRandomNumber()
    ));

    console.log(`Randomly generated ${Object.keys(value).length} data points`);

    return value;
};

