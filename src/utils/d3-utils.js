import * as d3 from 'd3';
import * as topojson from 'topojson';
import * as d3ScaleChromatic from 'd3-scale-chromatic';
import { scaleLinear } from 'd3-scale';
import { interpolateHcl, interpolate } from 'd3-interpolate';
import { hexbin as d3Hexbin } from 'd3-hexbin';
import { randomHelper } from './generate-data-utils';

import us from '../data/us.json';

const WIDTH = 1000;
const HEIGHT = 630;
const HEXAGON_RADIUS = 7;

// Global hexbin
const hexbin = d3Hexbin()
    .size([WIDTH, HEIGHT])
    .radius(HEXAGON_RADIUS);

const getImage = (path, callback) => {
    const image = new Image();
    image.src = path;
    image.onload = () => callback(image);
};

const loop = (f, t) => {
    f();
    setTimeout(() => {
        loop(f, t);
    }, t);
};

const drawStates = (svg, path) => {
    svg.append('path')
        .datum(topojson.mesh(us, us.objects.states, (a, b) => a !== b))
        .attr('class', 'states')
        .attr('fill', 'none')
        .attr('stroke', '#fff')
        .attr('stroke-linjoin', 'round')
        .attr('d', path);
};

const drawCounties = (svg, path, data, opt = {}) => {
    // color selector
    let color;
    if (!opt.colorSelector) {
        color = d3.scaleThreshold()
            .domain(d3.range(3, 15))
            .range(d3ScaleChromatic.schemeGreys[9]);
    } else {
        color = opt.colorSelector;
    }

    const drawer = svg.append('g')
        .attr('class', 'counties')
        .selectAll('path')
        .data(topojson.feature(us, us.objects.counties).features)
        .enter()
        .append('path')
        .attr('fill', d => color(d.rate = data[d.id]))
        .attr('d', path);

    if (opt.addTransition) {
        let flip = true;
        const toggle = () => {
            flip = !flip;
            drawer
                .transition()
                .duration(4200)
                .attr('fill', d => color(d.rate = flip ? data[d.id] + randomHelper(1, 2) : data[d.id] - randomHelper(1, 2)));
        };
        loop(toggle, 4200);
    }
};

const drawSvgHexbinMap = (data, opt) => {
    // Generate hexagons from pixel points
    const hexagons = hexbin(data);
    hexagons.forEach(d => {
        d.min = d3.min(d, p => p[2]);
        d.max = d3.max(d, p => p[2]);
        d.mean = d3.mean(d, p => p[2]);
    });

    const color = scaleLinear()
        .domain([0, 14, 15, 35, 132])
        .range(['#fff', '#222', '#d7191c', '#FFF59D', '#2c7bb6'])
        .interpolate(interpolateHcl);

    const svg = d3.select('svg')
      .attr('width', WIDTH)
      .attr('height', HEIGHT);

    const hexagon = svg.append('g')
        .attr('class', 'hexagons')
        .selectAll('path')
            .data(hexagons.filter(d => d.mean !== 0 || !(d.max < 14 && d.min > 12)))
        .enter()
        .append('path')
        .attr('d', hexbin.hexagon(HEXAGON_RADIUS))
        .attr('transform', d => `translate(${d.x},${d.y})`)
        .style('fill', d => color(d.min));

    if (opt.addTransition) {
        let flip = true;

        const toggle = () => {
            flip = !flip;
            hexagon
                .transition()
                .duration(5000)
                .style('fill', d => color(flip ? d.min : d.max))
                .styleTween('fill', d => {
                    const styleInterpolate = interpolate(flip ? d.min : d.max, flip ? d.max : d.min);
                    return t => color(styleInterpolate(t));
                });
        };

        loop(toggle, 5000);
    }
};

// convert svg map to canvas static image
// load the static image and convert it to hexagon svg map
export const convertToHexagon = (svg, opt) => {
    const points = [];

    // use the svg as the image
    const svgXml = (new XMLSerializer()).serializeToString(svg.node());
    const url = `data:image/svg+xml;base64,${btoa(svgXml)}`;

    // create new canvas
    const canvas = d3.select(document.createElement('canvas'))
        .attr('width', WIDTH)
        .attr('height', HEIGHT)
        .attr('id', 'mapCanvas');

    // get context from canvas
    const context = canvas.node().getContext('2d');

    getImage(url, image => {
        // Draw image into canvas
        context.drawImage(image, 50, 0, WIDTH, HEIGHT);
        image = context.getImageData(0, 0, WIDTH, HEIGHT);

        let min = 0;
        let max = 0;
        // rescale the colors
        for (let i = 0, n = WIDTH * HEIGHT * 4, d = image.data; i < n; i += 4) {
            min = Math.min(d[i], min);
            max = Math.max(d[i], max);
            points.push([i/4%WIDTH, Math.floor(i/4/WIDTH), d[i]]);
        }

        drawSvgHexbinMap(points, opt);
    });
};

// Method to draw a simple svg map with counties and states
export const drawSimpleSvgMap = (selector, data, opt) => {
    // create a 'virtual' svg to draw the graph but don't actually render it to DOM
    // We will pass this to the canvas for futhur modification
    const svg = selector
        .attr('id', 'original-map')
        .attr('width', WIDTH)
        .attr('height', HEIGHT)
        .attr('x', '100');

    // create draw path
    const path = d3.geoPath();

    drawCounties(svg, path, data, opt);
    if (!opt.noState) {
        drawStates(svg, path); // draw us land, states, and counties
    }
};

// Draw a simple svg map
export const drawSvgMap = (data, opt) => {
    const selector = d3.select('svg');
    selector.selectAll('*').remove();

    const colorSelector = d3.scaleThreshold()
        .domain(d3.range(randomHelper(4, 7), randomHelper(10, 15)))
        .range(d3ScaleChromatic.schemeSpectral[9]);

    drawSimpleSvgMap(selector, data, { ...opt, colorSelector });
};

export const drawHexbinMap = (data, opt) => {
    d3.select('svg').selectAll('*').remove();
    // Dont actually append it to the DOM
    const selector = d3.select(document.createElementNS('http://www.w3.org/2000/svg', 'svg'));
    drawSimpleSvgMap(selector, data, { ...opt, noState: true });
    convertToHexagon(selector, opt);
};
