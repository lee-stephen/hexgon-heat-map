import React, { Component } from 'react';
import { Provider } from 'react-redux';

export default StoreConstructor => App => class extends Component {
    render() {
        return (
            <Provider store={StoreConstructor()}>
                <App {...this.props} />
            </Provider>
        );
    }
};
