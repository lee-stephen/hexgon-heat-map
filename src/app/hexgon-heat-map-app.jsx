import React, { Component, PropTypes } from 'react';

// container
import hexgonHeadMapAppContainer from '../container/hexgon-heat-map-app';

import HexgonMap from '../components/hexgon-map';
import Button from '../components/button';

class HexgonHeatMapApp extends Component {
    static propTypes = {
        generateNewData: PropTypes.func.isRequired,
        toggleTransition: PropTypes.func.isRequired,
        toggleMap: PropTypes.func.isRequired
    }

    render() {
        return (
            <div>
                <HexgonMap {...this.props} />
                <Button
                    label={'Generate New Map'}
                    onClick={this.props.generateNewData}
                    style={{ marginRight: '20px' }}
                />
                <Button
                    label={'Toggle Animation'}
                    onClick={this.props.toggleTransition}
                    style={{ marginRight: '20px' }}
                />

                <Button
                    label={'Toggle Maps'}
                    onClick={this.props.toggleMap}
                    style={{ marginRight: '20px' }}
                />
            </div>
        );
    }
}

export default hexgonHeadMapAppContainer(HexgonHeatMapApp);
