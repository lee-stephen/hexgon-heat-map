import React from 'react';
import { render } from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import HexgonHeadMapApp from '../app/hexgon-heat-map-app';

const renderApp = Component => render((
    <AppContainer>
        <MuiThemeProvider>
            <Component />
        </MuiThemeProvider>
    </AppContainer>
), document.getElementById('app-container')); // eslint-disable-line no-undef

renderApp(HexgonHeadMapApp);

// Hot Module Replacement API
if (module.hot) {
    module.hot.accept('../app/hexgon-heat-map-app', () => {
        const updatedApp = require('../app/hexgon-heat-map-app').default; // eslint-disable-line global-require
        renderApp(updatedApp);
    });
}

