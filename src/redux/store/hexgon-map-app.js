import { createStore, combineReducers } from 'redux';
import mapData from '../reducers/map-data';

export default initialState => (
    createStore(combineReducers({
        mapData
    }), initialState)
);
