import { generateRandomIdValue } from '../../utils/generate-data-utils';

export const GENERATE_MAP_DATA = 'GENERATE_MAP_DATA';
export const TOGGLE_MAP_TRANSITION = 'TOGGLE_MAP_TRANSITION';
export const TOGGLE_MAP = 'TOGGLE_MAP';

const initialState = {
    mapData: generateRandomIdValue(),
    addTransition: true,
    showSvgMap: false
};

export default (state = initialState, action) => {
    switch (action.type) {
    case GENERATE_MAP_DATA:
        return Object.assign({}, state, {
            mapData: generateRandomIdValue()
        });

    case TOGGLE_MAP_TRANSITION:
        return Object.assign({}, state, {
            addTransition: !state.addTransition
        });

    case TOGGLE_MAP:
        return Object.assign({}, state, {
            showSvgMap: !state.showSvgMap
        });

    default:
        return state;
    }
};
