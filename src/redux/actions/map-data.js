import {
    GENERATE_MAP_DATA,
    TOGGLE_MAP_TRANSITION,
    TOGGLE_MAP
} from '../reducers/map-data';

export const generateMapData = () => ({ type: GENERATE_MAP_DATA });

export const toggleTransition = () => ({ type: TOGGLE_MAP_TRANSITION });

export const toggleMap = () => ({ type: TOGGLE_MAP });
