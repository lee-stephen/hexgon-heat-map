import React, { Component, PropTypes } from 'react';

import MapSvg from './map-svg';
import { drawSvgMap, drawHexbinMap } from '../utils/d3-utils';

export default class HexgonMap extends Component {
    static propTypes = {
        addTransition: PropTypes.bool.isRequired,
        mapData: PropTypes.object.isRequired,
        showSvgMap: PropTypes.bool.isRequired
    }

    componentDidMount() {
        this.drawMap();
    }

    componentDidUpdate() {
        this.drawMap();
    }

    drawMap() {
        if (this.props.showSvgMap) {
            drawSvgMap(this.props.mapData, { addTransition: this.props.addTransition });
        } else {
            drawHexbinMap(this.props.mapData, { addTransition: this.props.addTransition });
        }
    }

    render = () => (
        <div className="map-wrapper">
            <MapSvg />
        </div>
    )
}
