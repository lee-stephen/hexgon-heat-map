import React, { Component, PropTypes } from 'react';
import RaisedButton from 'material-ui/RaisedButton';

export default class Button extends Component {
    static propTypes = {
        label: PropTypes.string.isRequired,
        onClick: PropTypes.func.isRequired
    }

    render() {
        return (
            <RaisedButton
                {...this.props}
                label={this.props.label}
                primary
                onClick={this.props.onClick}
            />
        );
    }
}
