const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const webpackMerge = require('webpack-merge');

// defined node env
const isDev = process.env.NODE_ENV === 'development';

// see https://webpack.github.io/docs/configuration.html for detailed docs
const baseConfig = {
    // working dir for resolving entryPoints
    context: __dirname,

    // entry point of the bundle
    entry: {
        client: [
            './src/clients/hexgon-heat-map-client.js',
            'babel-polyfill'
        ]
    },

    // any .js or .jsx getting bundled gets run through the 'jsx' and 'babel-loader'
    module: {
        rules: [
            {
                test: /\.js$|\.jsx$/,
                loader: 'babel-loader'
            },
            {
                test: /\.(ico|jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2)(\?.*)?$/,
                loader: 'file-loader',
                query: {
                    name: '[name].[ext]',
                }
            },
            {
                test: /\.(less|css)$/,
                loader: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    loader: [
                        { loader: 'css-loader' },
                        {
                            loader: 'postcss-loader',
                            options: {
                                plugins: [
                                    require('autoprefixer')() // eslint-disable-line global-require
                                ]
                            }
                        },
                        { loader: 'less-loader' }
                    ],
                    publicPath: '/public/'
                })
            }
        ]
    },

    // where to put compiled webpacks
    output: {
        path: `${__dirname}/dist/public`,
        filename: 'hexgon-heat-map.js',
        publicPath: '/public/'
    },

    plugins: [
        // https://github.com/webpack/extract-text-webpack-plugin
        // Output less files into dist/public/css/
        new ExtractTextPlugin('css/style.css'),
        new webpack.NamedModulesPlugin(),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'libs',
            filename: 'libs.js'
        })
    ],

    resolve: {
        extensions: ['.jsx', '.js', '.json', '.css', '.less']
    }
};

// local dev config
const devConfig = {
    devtool: 'cheap-module-source-map',

    entry: {
        libs: [
            'react-hot-loader/patch',
            'webpack-hot-middleware/client?path=/__webpack_hmr&timeout=20000'
        ]
    },

    module: {
        rules: [
            {
                test: /\.js$|\.jsx$/,
                loader: 'babel-loader',
                query: {
                    // https://github.com/babel/babel-loader#options
                    cacheDirectory: true,
                    plugins: [
                        // Adds component stack to warning messages
                        // https://github.com/babel/babel/tree/master/packages/babel-plugin-transform-react-jsx-source
                        'transform-react-jsx-source',
                        // Adds __self attribute to JSX which React will use for some warnings
                        // https://github.com/babel/babel/tree/master/packages/babel-plugin-transform-react-jsx-self
                        'transform-react-jsx-self',
                        'react-hot-loader/babel'
                    ]
                }
            }
        ]
    },
    plugins: [
        new webpack.LoaderOptionsPlugin({
            debug: true
        }),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoEmitOnErrorsPlugin()
    ]
};

// production webpack config
const prodConfig = {
    devtool: false,
    plugins: [
        // https://webpack.github.io/docs/list-of-plugins.html#defineplugin
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: JSON.stringify('production')
            }
        }),
        new webpack.LoaderOptionsPlugin({
            minimize: true,
            debug: false
        }),
        // https://webpack.github.io/docs/list-of-plugins.html#uglifyjsplugin
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false,
                screw_ie8: false,
                conditionals: true,
                unused: true,
                comparisons: true,
                sequences: true,
                dead_code: true,
                evaluate: true,
                if_return: true,
                join_vars: true
            },
            output: {
                comments: false
            }
        })
    ]
};

const webpackConfig = webpackMerge.smart(
    baseConfig,
    !process.env.NODE_ENV || isDev ? devConfig : prodConfig
);

module.exports = webpackConfig;
